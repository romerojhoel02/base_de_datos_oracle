# 7. Borrar registros (delete)

### Ejercicio 01

#### Trabaje con la tabla "agenda" que registra la información referente a sus amigos.

##### 1. Elimine la tabla.

```sql
drop table agenda;
```
Salida del Script

```sh
Table AGENDA borrado.
```
##### 2. Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
Salida del Script

```sh
Table AGENDA creado.
```
##### 3. Ingrese los siguientes registros (insert into):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

##### 4. Elimine el registro cuyo nombre sea "Juan" (1 registro)
```sql
delete from agenda where nombre='Juan';
```
Salida del Script

```sh
1 fila eliminado
```
##### 5. Elimine los registros cuyo número telefónico sea igual a "4545454" (2 registros)

```sql
delete from agenda where telefono='4545454';
```
Salida del Script

```sh
2 filas eliminado
```
##### 6. Elimine todos los registros (2 registros)

```sql
delete from agenda;
```
Salida del Script

```sh
2 filas eliminado
```
### Ejercicio 02

#### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

##### 1. Elimine "articulos"

```sql
drop table articulos;
```
Salida del Script

```sh
Table ARTICULOS borrado.
```
##### 2. Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);
```
Salida del Script

```sh
Table ARTICULOS creado.
```

##### 3. Vea la estructura de la tabla.

```sql
DESCRIBE articulos;
```
Salida del Script

```sh
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(4)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2)  
CANTIDAD           NUMBER(3)    
```
##### 4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Elimine los artículos cuyo precio sea mayor o igual a 500 (2 registros)

```sql
delete from articulos where precio>=500;
```
Salida del Script

```sh
2 filas eliminado
```
##### 6. Elimine todas las impresoras (1 registro)

```sql
delete from articulos where nombre='impresora';
```
Salida del Script

```sh
1 fila eliminado
```
##### 7. Elimine todos los artículos cuyo código sea diferente a 4 (1 registro)

```sql
delete from articulos where codigo<>4;
```
Salida del Script

```sh
1 fila eliminado
```