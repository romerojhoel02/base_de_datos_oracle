# 37. Restricción primary key

## Ejercicios propuestos

## Ejercicio 01

Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla:

```sql
drop table empleados;
```

2. Créela con la siguiente estructura:

```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);
```

3. Ingrese algunos registros, dos de ellos con el mismo número de documento:
```sql
insert into empleados (documento, nombre, seccion)
values ('12345678', 'Juan Pérez', 'Ventas');

insert into empleados (documento, nombre, seccion)
values ('23456789', 'María López', 'Contabilidad');

insert into empleados (documento, nombre, seccion)
values ('12345678', 'Pedro Gómez', 'Recursos Humanos');

```
4. Intente establecer una restricción "primary key" para la tabla para que el documento no se repita ni admita valores nulos.
No lo permite porque la tabla contiene datos que no cumplen con la restricción, debemos eliminar (o modificar) el registro que tiene documento duplicado.

5. Establecezca la restricción "primary key" del punto 4
```sql
alter table empleados
add constraint pk_documento primary key (documento);

```
6. Intente actualizar un documento para que se repita.
No lo permite porque va contra la restricción.
```sql
update empleados
set documento = '23456789'
where nombre = 'Pedro Gómez';
-- Oracle mostrará un mensaje de error indicando que la restricción ha sido violada.

```
7. Intente establecer otra restricción "primary key" con el campo "nombre".
```sql
alter table empleados
add constraint pk_nombre primary key (nombre);
-- Oracle mostrará un mensaje de error indicando que ya existe una restricción "primary key" en la tabla.

```
8. Vea las restricciones de la tabla "empleados" consultando el catálogo "user_constraints" (1 restricción "P")
```sql
select constraint_name, constraint_type
from user_constraints
where table_name = 'EMPLEADOS';

```
9. Consulte el catálogo "user_cons_columns"
```sql
select constraint_name, column_name
from user_cons_columns
where table_name = 'EMPLEADOS';

```
## Ejercicio 02

Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".

1. Elimine la tabla:

```sql
drop table remis;
```

2. Cree la tabla con la siguiente estructura:

```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);
```

3. Ingrese algunos registros sin repetir patente y repitiendo número.
```sql
insert into remis(numero, patente, marca, modelo)
values (1, 'ABC123', 'Toyota', '2022');

insert into remis(numero, patente, marca, modelo)
values (2, 'DEF456', 'Ford', '2021');

insert into remis(numero, patente, marca, modelo)
values (3, 'GHI789', 'Chevrolet', '2020');

```
4. Ingrese un registro con patente nula.
```sql
insert into remis(numero, patente, marca, modelo)
values (4, null, 'Honda', '2019');

```
5. Intente definir una restricción "primary key" para el campo "numero".
```sql
alter table remis
add constraint pk_numero primary key (numero);
-- Oracle mostrará un mensaje de error indicando que la restricción no se puede establecer debido a valores duplicados en el campo "numero".

```
6. Intente establecer una restricción "primary key" para el campo "patente".
```sql
alter table remis
add constraint pk_patente primary key (patente);
-- Oracle mostrará un mensaje de error indicando que la restricción no se puede establecer debido a valores nulos en el campo "patente".

```
7. Modifique el valor "null" de la patente.
```sql
update remis
set patente = 'JKL012'
where numero = 4;

```
8. Establezca una restricción "primary key" del punto 6.
```sql
alter table remis
add constraint pk_patente primary key (patente);

```
9. Vea la información de las restricciones consultando "user_constraints" (1 restricción "P")
```sql
select constraint_name, constraint_type
from user_constraints
where table_name = 'REMIS';

```
10. Consulte el catálogo "user_cons_columns" y analice la información retornada (1 restricción)
```sql
select constraint_name, column_name
from user_cons_columns
where table_name = 'REMIS';

```