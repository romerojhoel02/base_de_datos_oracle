# 24. Operadores lógicos (and - or - not)


## Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla llamada "medicamentos" de una farmacia.

1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table medicamentos;

create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);
```

3. Ingrese algunos registros:

```sql
insert into medicamentos
values(100,'Sertal','Roche',5.2,100);

insert into medicamentos
values(102,'Buscapina','Roche',4.10,200);

insert into medicamentos
values(205,'Amoxidal 500','Bayer',15.60,100);

insert into medicamentos
values(230,'Paracetamol 500','Bago',1.90,200);

insert into medicamentos
values(345,'Bayaspirina','Bayer',2.10,150);

insert into medicamentos
values(347,'Amoxidal jarabe','Bayer',5.10,250);
```

4. Recupere los códigos y nombres de los medicamentos cuyo laboratorio sea "Roche' y cuyo precio sea menor a 5 (1 registro cumple con ambas condiciones)
```sql
select codigo, nombre
from medicamentos
where laboratorio = 'Roche' and precio < 5;

```
5. Recupere los medicamentos cuyo laboratorio sea "Roche" o cuyo precio sea menor a 5 (4 registros)
```sql
select *
from medicamentos
where laboratorio = 'Roche' or precio < 5;

```
6. Muestre todos los medicamentos cuyo laboratorio NO sea "Bayer" y cuya cantidad sea=100. Luego muestre todos los medicamentos cuyo laboratorio sea "Bayer" y cuya cantidad NO sea=100
```sql
-- Laboratorio NO sea "Bayer" y cantidad sea 100
select *
from medicamentos
where laboratorio != 'Bayer' and cantidad = 100;

-- Laboratorio sea "Bayer" y cantidad NO sea 100
select *
from medicamentos
where laboratorio = 'Bayer' and cantidad != 100;

```
7. Recupere los nombres de los medicamentos cuyo precio esté entre 2 y 5 inclusive (2 registros)
```sql
select nombre
from medicamentos
where precio between 2 and 5;

```
8. Elimine todos los registros cuyo laboratorio sea igual a "Bayer" y su precio sea mayor a 10 (1 registro eliminado)
```sql
delete from medicamentos
where laboratorio = 'Bayer' and precio > 10;

```
9. Cambie la cantidad por 200, de todos los medicamentos de "Roche" cuyo precio sea mayor a 5 (1 registro afectado)
```sql
update medicamentos
set cantidad = 200
where laboratorio = 'Roche' and precio > 5;

```
10. Muestre todos los registros para verificar el cambio.
```sql
select *
from medicamentos;

```
11. Borre los medicamentos cuyo laboratorio sea "Bayer" o cuyo precio sea menor a 3 (3 registros borrados)
```sql
delete from medicamentos
where laboratorio = 'Bayer' or precio < 3;

```
## Ejercicio 02

Trabajamos con la tabla "peliculas" de un video club que alquila películas en video.

1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table peliculas;

create table peliculas(
    codigo number(4),
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3),
    primary key (codigo)
);
```

2. Ingrese algunos registros:

```sql
insert into peliculas
values(1020,'Mision imposible','Tom Cruise',120);

insert into peliculas
values(1021,'Harry Potter y la piedra filosofal','Daniel R.',180);

insert into peliculas
values(1022,'Harry Potter y la camara secreta','Daniel R.',190);

insert into peliculas
values(1200,'Mision imposible 2','Tom Cruise',120);

insert into peliculas
values(1234,'Mujer bonita','Richard Gere',120);

insert into peliculas
values(900,'Tootsie','D. Hoffman',90);

insert into peliculas
values(1300,'Un oso rojo','Julio Chavez',100);

insert into peliculas
values(1301,'Elsa y Fred','China Zorrilla',110);
```

3. Recupere los registros cuyo actor sea "Tom Cruise" o "Richard Gere" (3 registros)
```sql
select *
from peliculas
where actor in ('Tom Cruise', 'Richard Gere');

```
4. Recupere los registros cuyo actor sea "Tom Cruise" y duración menor a 100 (ninguno cumple ambas condiciones)
```sql
select *
from peliculas
where actor = 'Tom Cruise' and duracion < 100;

```
5. Recupere los nombres de las películas cuya duración se encuentre entre 100 y 120 minutos(5 registros)
```sql
select titulo
from peliculas
where duracion between 100 and 120;

```
6. Cambie la duración a 200, de las películas cuyo actor sea "Daniel R." y cuya duración sea 180 (1 registro afectado)
```sql
update peliculas
set duracion = 200
where actor = 'Daniel R.' and duracion = 180;

```
7. Recupere todos los registros para verificar la actualización anterior
```sql
select *
from peliculas;

```
8. Borre todas las películas donde el actor NO sea "Tom Cruise" y cuya duración sea mayor o igual a 100 (2 registros eliminados)
```sql
delete from peliculas
where actor != 'Tom Cruise' and duracion >= 100;

```