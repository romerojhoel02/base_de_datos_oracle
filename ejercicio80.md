```sql
drop table libros;
drop table editoriales;

create table libros( 
  codigo number(5),
  titulo varchar2(40) not null,
  autor varchar2(30),
  editorial varchar2(20),
  precio number(5,2),
  primary key(codigo)
 ); 

insert into libros values(1,'Uno','Richard Bach','Planeta',15);
insert into libros values(2,'El aleph','Borges','Emece',25);
insert into libros values(3,'Matematica estas ahi','Paenza','Nuevo siglo',18);
insert into libros values(4,'Aprenda PHP','Mario Molina','Nuevo siglo',45);
insert into libros values(5,'Ilusiones','Richard Bach','Planeta',14);
insert into libros values(6,'Java en 10 minutos','Mario Molina','Nuevo siglo',50);

create table editoriales as
  (select distinct editorial as nombre
   from libros);

select * from editoriales;

drop table cantidadporeditorial;

create table cantidadporeditorial as
  (select editorial as nombre,count(*) as cantidad
  from libros
  group by editorial);

select *from cantidadporeditorial;

drop table ofertas20;

create table ofertas20 as
  (select *from libros
  where precio<=20)
  order by precio desc;

select *from ofertas20;

 
alter table editoriales add ciudad varchar2(30);

update editoriales set ciudad='Cordoba' where nombre='Planeta';
update editoriales set ciudad='Cordoba' where nombre='Emece';
update editoriales set ciudad='Buenos Aires' where nombre='Nuevo siglo';

drop table librosdecordoba;


create table librosdecordoba as
  (select titulo,autor from libros
  join editoriales
  on editorial=nombre 
  where ciudad='Cordoba');

select * from librosdecordoba;

```