# 72. Subconsultas como expresion

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla y la creamos:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(5,2)
);
```

Ingresamos los siguientes registros:

```sql
insert into libros
values(1,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',20.00);

insert into libros
values(2,'Alicia en el pais de las maravillas','Lewis Carroll','Plaza',35.00);

insert into libros
values(3,'Aprenda PHP','Mario Molina','Siglo XXI',40.00);

insert into libros
values(4,'El aleph','Borges','Emece',10.00);

insert into libros
values(5,'Ilusiones','Richard Bach','Planeta',15.00);

insert into libros
values(6,'Java en 10 minutos','Mario Molina','Siglo XXI',50.00);

insert into libros
values(7,'Martin Fierro','Jose Hernandez','Planeta',20.00);

insert into libros
values(8,'Martin Fierro','Jose Hernandez','Emece',30.00);

insert into libros
values(9,'Uno','Richard Bach','Planeta',10.00);
```

Obtenemos el título, precio de un libro específico y la diferencia entre su precio y el máximo valor:

```sql
select titulo, precio, precio-(select max(precio) from libros) as diferencia
from libros
where titulo='Uno';
```

Mostramos el título y precio del libro más costoso:

```sql
select titulo, autor, precio
from libros
where precio=(select max(precio) from libros);
```

Actualizamos el precio del libro con máximo valor:

```sql
update libros set precio=45
where precio=(select max(precio) from libros);
```

Eliminamos los libros con precio menor:

```sql
delete from libros
where precio=(select min(precio) from libros);
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(5,2)
);

insert into libros values(1,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',20.00);
insert into libros values(2,'Alicia en el pais de las maravillas','Lewis Carroll','Plaza',35.00);
insert into libros values(3,'Aprenda PHP','Mario Molina','Siglo XXI',40.00);
insert into libros values(4,'El aleph','Borges','Emece',10.00);
insert into libros values(5,'Ilusiones','Richard Bach','Planeta',15.00);
insert into libros values(6,'Java en 10 minutos','Mario Molina','Siglo XXI',50.00);
insert into libros values(7,'Martin Fierro','Jose Hernandez','Planeta',20.00);
insert into libros values(8,'Martin Fierro','Jose Hernandez','Emece',30.00);
insert into libros values(9,'Uno','Richard Bach','Planeta',10.00);

 -- Obtenemos el título, precio de un libro específico y la diferencia entre su precio y el máximo valor:
select titulo, precio, precio-(select max(precio) from libros) as diferencia
from libros
where titulo='Uno';

 -- Mostramos el título y precio del libro más costoso:
select titulo, autor, precio
from libros
where precio=(select max(precio) from libros);

 -- Actualizamos el precio del libro con máximo valor:
update libros set precio=45
where precio=(select max(precio) from libros);

-- Eliminamos los libros con precio menor:
delete from libros
where precio=(select min(precio) from libros);
```

## Ejercicios propuestos

Un profesor almacena el documento, nombre y la nota final de cada alumno de su clase en una tabla llamada "alumnos".

1. Elimine la tabla:

```sql
drop table alumnos;
```

2. Créela con los campos necesarios. Agregue una restricción "primary key" para el campo "documento" y una "check" para validar que el campo "nota" se encuentre entre los valores 0 y 10:

```sql
create table alumnos(
  .documento char(8),
  .nombre varchar2(30),
  .nota number(4,2),
  .primary key(documento),
  .constraint CK_alumnos_nota_valores check (nota>=0 and nota <=10)
);
```

3. Ingrese algunos registros:

```sql
insert into alumnos values('30111111','Ana Algarbe',5.1);
insert into alumnos values('30222222','Bernardo Bustamante',3.2);
insert into alumnos values('30333333','Carolina Conte',4.5);
insert into alumnos values('30444444','Diana Dominguez',9.7);
insert into alumnos values('30555555','Fabian Fuentes',8.5);
insert into alumnos values('30666666','Gaston Gonzalez',9.70);
```

4. Obtenga todos los datos de los alumnos con la nota más alta, empleando subconsulta
```sql
-- Obtener datos de los alumnos con la nota más alta
SELECT *
FROM alumnos
WHERE nota = (
  SELECT MAX(nota)
  FROM alumnos
);

```
5. Realice la misma consulta anterior pero intente que la consulta interna retorne, además del máximo valor de nota, el nombre del alumno.
Mensaje de error, porque la lista de selección de una subconsulta que va luego de un operador de comparación puede incluir sólo un campo o expresión (excepto si se emplea "exists" o "in").
```sql
-- Error: No se puede incluir el nombre en la subconsulta que va después del operador de comparación
SELECT *
FROM alumnos
WHERE (nota, nombre) = (
  SELECT MAX(nota), nombre
  FROM alumnos
);

```
6. Muestre los alumnos que tienen una nota menor al promedio, su nota, y la diferencia con el promedio.
```sql
-- Mostrar alumnos con nota menor al promedio y su diferencia
SELECT nombre, nota, nota - AVG(nota) OVER () AS diferencia
FROM alumnos
WHERE nota < (
  SELECT AVG(nota)
  FROM alumnos
);

```
7. Cambie la nota del alumno que tiene la menor nota por 4.
```sql
-- Actualizar la nota del alumno con la menor nota a 4
UPDATE alumnos
SET nota = 4
WHERE nota = (
  SELECT MIN(nota)
  FROM alumnos
);

```
8. Elimine los alumnos cuya nota es menor al promedio.
```sql
-- Eliminar los alumnos con nota menor al promedio
DELETE FROM alumnos
WHERE nota < (
  SELECT AVG(nota)
  FROM alumnos
);

```