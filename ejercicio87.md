# 87. Vistas (with check option)

## Practica de laboratorio

Una empresa almacena la información de sus empleados en una tabla llamada "empleados".

Eliminamos la tabla:

```sql
drop table empleados;
```

Creamos la tabla:

```sql
create table empleados(
    documento char(8),
    sexo char(1)
    constraint CK_empleados_sexo check (sexo in ('f','m')),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    cantidadhijos number(2),
    constraint CK_empleados_hijos check (cantidadhijos>=0),
    estadocivil char(10)
    constraint CK_empleados_estadocivil check (estadocivil in ('casado','divorciado','soltero','viudo'))
);
```

Ingresamos algunos registros:

```sql
insert into empleados values('22222222','f','Lopez','Ana','Administracion',2,'casado');
insert into empleados values('23333333','m','Lopez','Luis','Administracion',0,'soltero');
insert into empleados values('24444444','m','Garcia','Marcos','Sistemas',3,'divorciado');
insert into empleados values('25555555','m','Gomez','Pablo','Sistemas',2,'casado');
insert into empleados values('26666666','f','Perez','Laura','Contaduria',3,'casado');
```

Creamos o reemplazamos (si existe) la vista "vista_empleados", para que muestre el nombre, apellido, sexo y sección de todos los empleados de "Administracion" agregando la cláusula "with check option" para evitar que se modifique la sección de tales empleados a través de la vista y que se ingresen empleados de otra sección:

```sql
create or replace view vista_empleados as
select apellido, nombre, sexo, seccion
from empleados
where seccion='Administracion'
with check option;
```

Consultamos la vista:

```sql
select *from vista_empleados;
```

Actualizarmos el nombre de un empleado a través de la vista:

```sql
 updat vista_empleados set nombre='Beatriz' where nombre='Ana';
```

Oracle aceptó la actualización porque el campo "nombre" no está restringido.

Veamos si la modificación se realizó en la tabla:

```sql
select *from empleados;
```

Intentamos actualizar la sección de un empleado a través de la vista:

```sql
update vista_empleados set seccion='Sistemas' where nombre='Beatriz';
```

Oracle no aceptó la actualización porque el campo "nombre" está restringido.

Ingresamos un registro mediante la vista:

```sql
insert into vista_empleados values('Gomez','Gabriela','f','Administracion');
```

Oracle acepta la inserción porque ingresamos un valor para "seccion" que incluirá el registro en la vista.

Intentamos ingresar un empleado de otra sección:

```sql
insert into vista_empleados values('Torres','Tatiana','f','Sistemas');
```

Oracle no acepta la inserción porque ingresamos un valor para "seccion" que excluirá el nuevo registro de la vista.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table empleados;

create table empleados(
    documento char(8),
    sexo char(1)
    constraint CK_empleados_sexo check (sexo in ('f','m')),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    cantidadhijos number(2),
    constraint CK_empleados_hijos check (cantidadhijos>=0),
    estadocivil char(10)
    constraint CK_empleados_estadocivil check (estadocivil in ('casado','divorciado','soltero','viudo'))
);

insert into empleados values('22222222','f','Lopez','Ana','Administracion',2,'casado');
insert into empleados values('23333333','m','Lopez','Luis','Administracion',0,'soltero');
insert into empleados values('24444444','m','Garcia','Marcos','Sistemas',3,'divorciado');
insert into empleados values('25555555','m','Gomez','Pablo','Sistemas',2,'casado');
insert into empleados values('26666666','f','Perez','Laura','Contaduria',3,'casado');

 -- Creamos o reemplazamos (si existe) la vista "vista_empleados", para que muestre el nombre,
 -- apellido, sexo y sección de todos los empleados de "Administracion" agregando la cláusula
 -- "with check option" para evitar que se modifique la sección de tales empleados
 -- a través de la vista y que se ingresen empleados de otra sección:
create or replace view vista_empleados as
select apellido, nombre, sexo, seccion
from empleados
where seccion='Administracion'
with check option;

 -- Consultamos la vista:
select *from vista_empleados;

 -- Actualizarmos el nombre de un empleado a través de la vista:
update vista_empleados set nombre='Beatriz' where nombre='Ana';
 -- Oracle aceptó la actualización porque el campo "nombre" no está restringido.

 -- Veamos si la modificación se realizó en la tabla:
select *from empleados;

 -- Intentamos actualizar la sección de un empleado a través de la vista:
update vista_empleados set seccion='Sistemas' where nombre='Beatriz';
 -- Oracle no aceptó la actualización porque el campo "nombre" está restringido.

 -- Ingresamos un registro mediante la vista:
insert into vista_empleados values('Gomez','Gabriela','f','Administracion');
 -- Oracle acepta la inserción porque ingresamos un valor para "seccion"
 -- que incluirá el registro en la vista.

-- Intentamos ingresar un empleado de otra sección:
insert into vista_empleados values('Torres','Tatiana','f','Sistemas');
```

## Ejercicios propuestos

Una empresa almacena la información de sus clientes en una tabla llamada "clientes".

1. Elimine la tabla:

```sql
drop table clientes;
```

2. Cree la tabla:

```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);
```

3. Ingrese algunos registros:

```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');
```

4. Cree o reemplace la vista "vista_clientes" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" sin emplear "with check option"
```sql
CREATE OR REPLACE VIEW vista_clientes AS
SELECT nombre, ciudad
FROM clientes
WHERE ciudad <> 'Cordoba';

```
5. Cree o reemplace la vista "vista_clientes2" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" empleando "with check option"
```sql
CREATE OR REPLACE VIEW vista_clientes2 AS
SELECT nombre, ciudad
FROM clientes
WHERE ciudad <> 'Cordoba'
WITH CHECK OPTION;

```
6. Consulte ambas vistas
```sql
SELECT * FROM vista_clientes;
SELECT * FROM vista_clientes2;

```
7. Intente modificar la ciudad del cliente "Pedro Perez" a "Cordoba" través de la vista que está restringida.
```sql
UPDATE vista_clientes2
SET ciudad = 'Cordoba'
WHERE nombre = 'Pedro Perez';
-- Esto debería arrojar un error debido a la restricción CHECK OPTION

```
8. Realice la misma modificación que intentó en el punto anterior a través de la vista que no está restringida
```sql
UPDATE vista_clientes
SET ciudad = 'Cordoba'
WHERE nombre = 'Pedro Perez';
-- Esto modificará el registro sin restricciones

```
9. Actualice la ciudad del cliente "Oscar Luque" a "Buenos Aires" mediante la vista restringida
```sql
UPDATE vista_clientes2
SET ciudad = 'Buenos Aires'
WHERE nombre = 'Oscar Luque';
-- Esto debería permitir la actualización

```
10. Verifique que "Oscar Luque" aún se incluye en la vista
```sql
SELECT * FROM vista_clientes2 WHERE nombre = 'Oscar Luque';
-- El registro debería seguir estando presente en la vista

```
11. Intente ingresar un empleado de "Cordoba" en la vista restringida
```sql
INSERT INTO vista_clientes2 (nombre, ciudad) VALUES ('Maria Martinez', 'Cordoba');
-- Esto debería arrojar un error debido a la restricción CHECK OPTION

```
12. Ingrese el empleado anterior a través de la vista no restringida
```sql
INSERT INTO vista_clientes (nombre, ciudad) VALUES ('Maria Martinez', 'Cordoba');
-- Esto permitirá ingresar el registro sin restricciones

```
13. Ingrese un empleado de "Salta" en la vista restringida
```sql
INSERT INTO vista_clientes2 (nombre, ciudad) VALUES ('Eduardo Sanchez', 'Salta');
-- Esto debería permitir el ingreso del registro

```
14. Verifique que el nuevo registro está incluido en la vista
```sql
SELECT * FROM vista_clientes2 WHERE nombre = 'Eduardo Sanchez';
-- El registro debería estar presente en la vista

```