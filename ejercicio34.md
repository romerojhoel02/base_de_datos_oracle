# 34. Secuencias (create sequence - currval - nextval - drop sequence)

## Ejercicios propuestos

## Ejercicio 01

Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla "empleados":

```sql
drop table empleados;
```

2. Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```

3. Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
```sql
-- Eliminar la secuencia si existe
begin
    execute immediate 'drop sequence sec_legajoempleados';
exception
    when others then
        if sqlcode != -2289 then
            raise;
        end if;
end;
/

-- Crear la secuencia
create sequence sec_legajoempleados
    minvalue 1
    maxvalue 999
    start with 100
    increment by 2
    nocycle;

-- Inicializar la secuencia
select sec_legajoempleados.nextval from dual;

```
4. Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');
```

5. Recupere los registros de "libros" para ver los valores de clave primaria.
```sql
select * from empleados;

```
Note que los valores se incrementaron en 2, porque así se estableció el valor de incremento al crear la secuencia.

6. Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.
```sql
select sec_legajoempleados.currval from dual;

```
7. Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.
```sql
select sec_legajoempleados.nextval from dual;

```
8. Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el próximo valor, emplee "currval" para almacenar el valor de legajo)
```sql
insert into empleados
values (sec_legajoempleados.currval, '29000111', 'Hector Huerta');

```
9. Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.
```sql
select * from empleados;

```
10. Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)
```sql
select sec_legajoempleados.nextval from dual;

```
11. Ingrese un empleado con valor de legajo "112".
```sql
insert into empleados
values (112, '30000222', 'Iván Ibarra');

```
12. Intente ingresar un registro empleando "currval":

```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');
```

Mensaje de error porque el legajo está repetido y la clave primaria no puede repetirse.

13. Incremente el valor de la secuencia. Retorna 114.
```sql
select sec_legajoempleados.nextval from dual;

```
14. Ingrese el registro del punto 11.
```sql
insert into empleados
values (sec_legajoempleados.currval, '30000222', 'Iván Ibarra');

```
Ahora si lo permite, pues el valor retornado por "currval" no está repetido en la tabla "empleados".

15. Recupere los registros.
```sql
select * from empleados;

```
16. Vea las secuencias existentes y analice la información retornada.
```sql
select sequence_name, min_value, max_value, last_number, increment_by, cycle_flag
from user_sequences;

```
Debe aparecer "sec_legajoempleados".

17. Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS".
```sql
select object_name, object_type
from user_objects
where object_name like '%EMPLEADOS%';

```
Debe aparacer la tabla "empleados" y la secuencia "sec_legajoempleados".

18. Elimine la secuencia creada.
```sql
-- Eliminar la secuencia
begin
    execute immediate 'drop sequence sec_legajoempleados';
exception
    when others then
        if sqlcode != -2289 then
            raise;
        end if;
end;
/

-- Confirmar la eliminación verificando si la secuencia todavía existe
select sequence_name
from user_sequences
where sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
19. Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
```sql
select object_name, object_type
from user_objects
where object_type = 'SEQUENCE'
and object_name = 'SEC_LEGAJOEMPLEADOS';

```