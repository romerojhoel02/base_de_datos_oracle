# 44. Indices (Crear . Información)

## Practica de laboratorio

Trabajamos con la tabla "empleados" de una librería.
Eliminamos la tabla y la creamos:

```sql
drop table empleados;

create table empleados(
    legajo number(5),
    documento char(8),
    apellido varchar2(25),
    nombre varchar2(25),
    domicilio varchar2(30)
);
```

Agregamos una restricción "primary key" sobre el campo "legajo":

```sql
alter table empleados
add constraint PK_empleados_legajo
primary key (legajo);
```

Consultamos "user_constraints":

```sql
select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';
```

Note que Oracle creó un índice con el mismo nombre de la restricción.

Veamos los índices de "empleados":

```sql
select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';
```

Aparece 1 fila, mostrando el nombre del índice, indicando que es normal y único.

Creamos un índice único sobre el campo "documento":

```sql
create unique index I_empleados_documento
on empleados(documento);
```

Verificamos que se creó el índice:

```sql
select index_name, index_type, uniqueness
from user_indexes where table_name='EMPLEADOS';
```

Aparecen 2 filas, una por cada índice.

Agregamos a la tabla una restricción única sobre el campo "documento":

```sql
alter table empleados
add constraint UQ_empleados_documento
unique(documento);
```

Analicemos la información que nos muestra "user_constraints":

```sql
select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';
```

En la columna "index_name" correspondiente a la restricción única, aparece "I_empleados_documento", Oracle usa para esta restricción el índice existente, no crea otro nuevo.

Creamos un índice no único, compuesto (para los campos "apellido" y "nombre"):

```sql
create index I_empleados_apellidonombre
on empleados(apellido,nombre);
```

Consultamos el diccionario "user_indexes":

```sql
select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';
```

Nos muestra información sobre los 3 índices de la tabla.

Veamos todos los índices de la base de datos activa consultando "user_objects":

```sql
select *from user_objects
where object_type='INDEX';
```

Aparecen varios índices, entre ellos, los de nuestra tabla "empleados".

Obtenemos información de "user_ind_columns":

```sql
select index_name,column_name,column_position
from user_ind_columns
where table_name='EMPLEADOS';
```

Nos muestra la siguiente tabla:

```sh
INDEX_NAME                       COLUMN_NAME      COLUMN_POSITION
-----------------------------------------------------------------
PK_EMPLEADOS_LEGAJO              LEGAJO           1
I_EMPLEADOS_DOCUMENTO            DOCUMENTO        1
I_EMPLEADOS_APELLIDONOMBRE       APELLIDO         1
I_EMPLEADOS_APELLIDONOMBRE       NOMBRE           2
```

La tabla tiene 3 índices, 2 filas corresponden al índice compuesto "I_empleados_apellidonombre"; la columna "position" indica el orden de los campos indexados.

Agregamos algunos registros:

```sql
insert into empleados values(1,'22333444','Lopez','Juan','Colon 123');
insert into empleados values(2,'23444555','Lopez','Luis','Lugones 1234');
insert into empleados values(3,'24555666','Garcia','Pedro','Avellaneda 987');
insert into empleados values(4,'25666777','Garcia','Ana','Caseros 678');
```

Si intentamos crear un índice único para el campo "apellido" (que contiene valores duplicados") Oracle no lo permite:

```sql
create unique index I_empleados_apellido
on empleados(apellido);
```

Igualmente, si hay un índice único sobre un campo y luego intentamos ingresar un registro con un valor repetido para el campo indexado, Oracle no lo permite.

Creamos un índice único sobre el campo "nombre":

```sql
create unique index I_empleados_nombre
on empleados(nombre);
```

Oracle lo permite porque no hay valores duplicados.

Intentamos agregamos un registro que repita un nombre:

```sql
insert into empleados values(5,'30111222','Perez','Juan','Bulnes 233');
```

Oracle no lo permite.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table empleados;

create table empleados(
    legajo number(5),
    documento char(8),
    apellido varchar2(25),
    nombre varchar2(25),
    domicilio varchar2(30)
);

alter table empleados
add constraint PK_empleados_legajo
primary key (legajo);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';

create unique index I_empleados_documento
on empleados(documento);

select index_name, index_type, uniqueness
from user_indexes where table_name='EMPLEADOS';

alter table empleados
add constraint UQ_empleados_documento
unique(documento);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

create index I_empleados_apellidonombre
on empleados(apellido,nombre);

select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';

select *from user_objects
where object_type='INDEX';

select index_name,column_name,column_position
from user_ind_columns
where table_name='EMPLEADOS';

insert into empleados values(1,'22333444','Lopez','Juan','Colon 123');
insert into empleados values(2,'23444555','Lopez','Luis','Lugones 1234');
insert into empleados values(3,'24555666','Garcia','Pedro','Avellaneda 987');
insert into empleados values(4,'25666777','Garcia','Ana','Caseros 678');

create unique index I_empleados_apellido
on empleados(apellido);

create unique index I_empleados_nombre
on empleados(nombre);

insert into empleados values(5,'30111222','Perez','Juan','Bulnes 233');
```

## Ejercicios de laboratorio

Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".
1. Setee el formato de "date" para que nos muestre hora y minutos:

alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
2. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);
```

3. Establezca una restricción "check" que admita solamente los valores "a" y "m" para el campo "tipo":

```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));
```

4. Agregue una restricción "primary key" que incluya los campos "patente" y "horallegada"
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
5. Ingrese un vehículo.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'a', SYSDATE);

```
6. Intente ingresar un registro repitiendo la clave primaria.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'm', SYSDATE);
-- Esto generará un error debido a la violación de la restricción de clave primaria

```
7. Ingrese un registro repitiendo la patente pero no la hora de llegada.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('ABC123', 'm', SYSDATE);
-- Esto se permitirá ya que la hora de llegada es diferente

```
8. Ingrese un registro repitiendo la hora de llegada pero no la patente.
```sql
INSERT INTO vehiculos (patente, tipo, horallegada)
VALUES ('XYZ789', 'a', SYSDATE);
-- Esto se permitirá ya que la patente es diferente

```
9. Vea todas las restricciones para la tabla "vehiculos"
aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
10. Elimine la restricción "primary key"
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT PK_vehiculos;

```
11. Vea si se ha eliminado.
Ahora aparecen 3 restricciones.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
12. Elimine la restricción de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints").
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT <nombre_restriccion>;
-- Reemplaza <nombre_restriccion> con el nombre de la restricción que establece que el campo "patente" no sea nulo

```
13. Vea si se han eliminado.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
14. Vuelva a establecer la restricción "primary key" eliminada.
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);
``

```
15. La playa quiere incluir, para el campo "tipo", además de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (camión). No puede modificar la restricción, debe eliminarla y luego redefinirla con los 3 valores.
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT CK_vehiculos_tipo;

ALTER TABLE vehiculos
ADD CONSTRAINT CK_vehiculos_tipo CHECK (tipo IN ('a', 'm', 'c'));

```
16. Consulte "user_constraints" para ver si la condición de chequeo de la restricción "CK_vehiculos_tipo" se ha modificado.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS' AND constraint_name = 'CK_vehiculos_tipo';

```