# ESTUDIANTE

## ALEXANDER JHOEL MAMANI ROMERO

[1. Crear tablas (create table - describe - all_tables - drop table)](ejercicio1.md)

[2. Ingresar registros (insert into- select)](ejercicio2.md)

[3. Tipos de datos](ejercicio3.md)

[4. Recuperar algunos campos (select)](ejercicio4.md)

[5. Recuperar algunos registros (where)](ejercicio5.mdmd)

[6. Operadores relacionales](ejercicio6.md)

[7. Borrar registros (delete)](ejercicio7.md)

[8. Actualizar registros (update)](ejercicio8.md)

[9. Comentarios](ejercicio9.md)

[10. Valores nulos (null)](ejercicio10.md)

[11. Operadores relacionales (is null)](ejercicio11.md)

[12. Clave primaria (primary key)](ejercicio12.md)

[13. Vaciar la tabla (truncate table)](ejercicio13.md)

[14. Tipos de datos alfanuméricos](ejercicio14.md)

[15. Tipos de datos numéricos](ejercicio15.md)

[16. Ingresar algunos campos](ejercicio16.md)

[17. Valores por defecto (default)](ejercicio17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](ejercicio18.md)

[19.  Alias (encabezados de columnas)](ejercicio19.md)

[20. Funciones string](ejercicio20.md)

[21. Funciones matemáticas](ejercicio21.md)

[22. Funciones de fechas y horas](ejercicio22.md)

[23. Ordenar registros (order by)](ejercicio23.md)

[24. Operadores lógicos (and - or - not)](ejercicio24.md)

[25. Otros operadores relacionales (between)](ejercicio25.md)

[26. Otros operadores relacionales (in)](ejercicio26.md)

[27. Búsqueda de patrones (like - not like)](ejercicio27.md)

[28. Contar registros (count)](ejercicio28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](ejercicio29.md)

[30. Agrupar registros (group by)](ejercicio30.md)

[31. Seleccionar grupos (Having)](ejercicio31.md)

[32. Registros duplicados (Distinct)](ejercicio32.md)

[33. Clave primaria compuesta](ejercicio33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](ejercicio34.md)

[35. Alterar secuencia (alter sequence)](ejercicio35.md)

[36. Integridad de datos](ejercicio36.md)

[37. Restricción primary key](ejercicio37.md)

[38. Restricción unique](ejercicio38.md)

[39. Restriccioncheck](ejercicio39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](ejercicio0.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](ejercicio41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](ejercicio42.md)

[43. Indices](ejercicio43.md)

[44. Indices (Crear . Información)](ejercicio44.md)

[45. Indices (eliminar)](ejercicio45.md)

[46. Varias tablas (join)](ejercicio46.md)

[47. Combinación interna (join)](ejercicio47.md)

[48. Combinación externa izquierda (left join)](ejercicio48.md)

[49. Combinación externa derecha (right join)](ejercicio49.md)

[50. Combinación externa completa (full join)](ejercicio50.md)

[51. Combinaciones cruzadas (cross)](ejercicio51.md)

[52. Autocombinación](ejercicio52.md)

[53. Combinaciones y funciones de agrupamiento](ejercicio53.md)

[54. Combinar más de 2 tablas](ejercicio54.md)

[55. Otros tipos de combinaciones](ejercicio55.md)

[56. Clave foránea](ejercicio56.md)

[57. Restricciones (foreign key)](ejercicio57.md)

[58. Restricciones foreign key en la misma tabla](ejercicio58.md)

[59. Restricciones foreign key (eliminación)](ejercicio59.md)

[60. Restricciones foreign key deshabilitar y validar](ejercicio60.md)

[61. Restricciones foreign key (acciones)](ejercicio61.md)

[62. Información de user_constraints](ejercicio62.md)

[63. Restricciones al crear la tabla](ejercicio63.md)

[64. Unión](ejercicio64.md)

[65. Intersección](ejercicio65.md)

[66. Minus](ejercicio66.md)

[67. Agregar campos (alter table-add)](ejercicio67.md)

[68. Modificar campos (alter table - modify)](ejercicio68.md)

[69. Eliminar campos (alter table - drop)](ejercicio69.md)

[70.  Agregar campos y restricciones (alter table)](ejercicio70.md)

[71. Subconsultas](ejercicio71.md)

[72. Subconsultas como expresion](ejercicio72.md)

[73. Subconsultas con in](ejercicio73.md)

[74. Subconsultas any- some - all](ejercicio74.md)

[75. Subconsultas correlacionadas](ejercicio75.md)

[76. Exists y No Exists](ejercicio76.md)

[77. Subconsulta simil autocombinacion](ejercicio77.md)

[78. Subconsulta conupdate y delete](ejercicio78.md)

[79. Subconsulta e insert](ejercicio79.md)

[80. Crear tabla a partir de otra (create table-select)](ejercicio80.md)

[81. Vistas (create view)](ejercicio81.md)

[82. Vistas (información)](ejercicio82.md)

[83. Vistas eliminar (drop view)](ejercicio83.md)

[84. Vistas (modificar datos a través de ella)](ejercicio84.md)

[85. Vistas (with read only)](ejercicio85.md)

[86. Vistas modificar (create or replace view)](ejercicio86.md)

[87. Vistas (with check option)](ejercicio87.md)

[88. Vistas (otras consideraciones: force)](ejercicio88.md)

[89. Vistas materializadas (materialized view)](ejercicio89.md)

[90. Procedimientos almacenados](ejercicio90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](ejercicio91.md)

[92. Procedimientos Almacenados (eliminar)](ejercicio92.md)

[93. Procedimientos almacenados (parámetros de entrada)](ejercicio93.md)

[94. Procedimientos almacenados (variables)](ejercicio94.md)

[95. Procedimientos Almacenados (informacion)](ejercicio95.md)

[96. Funciones](ejercicio96.md)

[97. Control de flujo (if)](ejercicio97.md)

[98. Control de flujo (case)](ejercicio98.md)

[99. Control de flujo (loop)](ejercicio99.md)

[100. Control de flujo (for)](ejercicio100.md)

[101. Control de flujo (while loop)](ejercicio101.md)

[102. Disparador (trigger)](ejercicio102.md)

[103. Disparador (información)](ejercicio103.md)

[104. Disparador de inserción a nivel de sentencia](ejercicio104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](ejercicio105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](ejercicio106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](ejercicio107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](ejercicio108.md)

[109. Disparador de actualización - lista de campos (update trigger)](ejercicio109.md)

[110. Disparador de múltiples eventos](ejercicio110.md)

[111. Disparador (old y new)](ejercicio111.md)

[112. Disparador condiciones (when)](ejercicio112.md)

[113. Disparador de actualizacion - campos (updating)](ejercicio113.md)

[114. Disparadores (habilitar y deshabilitar)](ejercicio114.md)

[115. Disparador (eliminar)](ejercicio115.md)
