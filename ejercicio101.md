# 101. Control de flujo (while loop)

## Practica de laboratorio

1. Mostramos la tabla del 3 hasta el 5. En primer lugar activamos el paquete "dbms_output" para poder emplear los procedimientos de dicho paquete, luego ejecutamos el procedimiento "dbms_output.enable" para habilitar las llamadas a los procedimientos y funciones de tal paquete, así podremos emplear la función de salida "dbms_output.put_line".

```sql
set serveroutput on;
execute dbms_output.enable (1000000);
```

2. Luego, declaramos la variable numérica "numero" y le asignamos el valor cero; tal variable contendrá el multiplicando. También declaramos la variable "resultado" de tipo numérico que almacenará el resultado de cada multiplicación. Comenzamos el bloque "begin... end" con la estructura repetitiva "while... loop". La condición chequea si el valor de la variable "numero" es menmor o igual a 5; las sentencias que se repetirán serán:

- multiplicar "numero" por 3 y asignárselo a "resultado",

- imprimir "resultado" y

- incrementar la variable "numero" para que la siguiente vez que se entre al bucle repetitivo se multiplique 3 por otro número.

```sql
declare
    numero number:=0;
    resultado number;
begin
    while numero<=5 loop
        resultado:=3*numero;
        dbms_output.put_line('3*'||to_char(numero)||'='||to_char(resultado));
        numero:=numero+1;
    end loop;
end;
```

3. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
set serveroutput on;
execute dbms_output.enable (1000000);

declare
    numero number:=0;
    resultado number;
begin
    while numero<=5 loop
        resultado:=3*numero;
        dbms_output.put_line('3*'||to_char(numero)||'='||to_char(resultado));
        numero:=numero+1;
    end loop;
end;
/
```
