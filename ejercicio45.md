# 45. Indices (eliminar)

## Practica de laboratorio

Trabajamos con la tabla "empleados".
Eliminamos la tabla y la creamos:

```sql
drop table empleados;

create table empleados(
    legajo number (5),
    documento char(8),
    apellido varchar2(40),
    nombre varchar2(40)
);
```

Creamos un índice único para el campo "legajo":

```sql
create unique index I_empleados_legajo
on empleados(legajo);
```

Agregamos una restricción "unique" sobre "legajo":

```sql
alter table empleados
add constraint UQ_empleados_legajo
unique (legajo);
```

Verificamos que la restricción usa el índice creado anteriormente, no crea otro:

```sql
select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';
```

Agregamos una restricción "primary key" sobre "documento":

```sql
alter table empleados
add constraint PK_empleados_documento
primary key(documento);
```

Verificamos que Oracle creó un índice para el campo "documento":

```sql
select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';
```

Consultamos todos los índices y sus tipos consultando "user_indexes":

```sql
select index_name,uniqueness
from user_indexes
where table_name='EMPLEADOS';
```

Creamos un índice no único sobre "nombre":

```sql
create index I_empleados_nombre
on empleados(nombre);
```

Creamos un índice no único sobre "apellido":

```sql
create index I_empleados_apellido
on empleados(apellido);
```

Si intentamos eliminar un índice que utiliza una restricción Oracle no lo permite:

```sql
drop index I_empleados_legajo;
```

Verificamos que tal índice es utilizado por una restricción:

```sql
select constraint_name, constraint_type, index_name
from user_constraints
where index_name='I_EMPLEADOS_LEGAJO';
```

Eliminamos el índice "I_empleados_nombre":

```sql
drop index I_empleados_nombre;
```

Corroboremos que se eliminó:

```sql
select *from user_objects
where object_type='INDEX';
```

No aparece en la lista.

Eliminamos la tabla:

```sql
drop table empleados;
```

Verificamos que se eliminaron todos los índices establecidos sobre la tabla:

```sql
select *from user_indexes where table_name='EMPLEADOS';
```

No aparece ninguno cuyo nombre de la tabla sea "empleados".

Lo verificamos nuevamente consultando el diccionario de todos los objetos:

```sql
select *from user_objects
where object_type='INDEX';
```

No aparecen los índices.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table empleados;

create table empleados(
    legajo number (5),
    documento char(8),
    apellido varchar2(40),
    nombre varchar2(40)
);

create unique index I_empleados_legajo
on empleados(legajo);

alter table empleados
add constraint UQ_empleados_legajo
  unique (legajo);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

alter table empleados
add constraint PK_empleados_documento
primary key(documento);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

select index_name,uniqueness
from user_indexes
where table_name='EMPLEADOS';

create index I_empleados_nombre
on empleados(nombre);

create index I_empleados_apellido
on empleados(apellido);

drop index I_empleados_legajo;

select constraint_name, constraint_type, index_name
from user_constraints
where index_name='I_EMPLEADOS_LEGAJO';

drop index I_empleados_nombre;

select *from user_objects
where object_type='INDEX';

drop table empleados;

select *from user_indexes where table_name='EMPLEADOS';

select *from user_objects
where object_type='INDEX';
```

## Ejercicios propuestos

Un profesor guarda algunos datos de sus alumnos en una tabla llamada "alumnos".

1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);
```

2. Cree un índice no único para el campo "nombre".
```sql
CREATE INDEX IDX_nombre ON alumnos(nombre);

```
3. Establezca una restricción "primary key" para el campo "legajo"
```sql
ALTER TABLE alumnos
ADD CONSTRAINT PK_alumnos_legajo PRIMARY KEY (legajo);

```
4. Verifique que se creó un índice con el nombre de la restricción.
```sql
SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';

```
5. Verifique que se creó un índice único con el nombre de la restricción consultando el diccionario de índices.
```sql
SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';

```
6. Intente eliminar el índice "PK_alumnos_legajo" con "drop index".
```sql
DROP INDEX PK_alumnos_legajo;

```
7. Cree un índice único para el campo "documento".
```sql
CREATE UNIQUE INDEX IDX_documento ON alumnos(documento);

```
8. Agregue a la tabla una restricción única sobre el campo "documento" y verifique que no se creó un índice, Oracle emplea el índice creado en el punto anterior.
```sql
ALTER TABLE alumnos
ADD CONSTRAINT UQ_alumnos_documento UNIQUE (documento);

```
9. Intente eliminar el índice "I_alumnos_documento" (no se puede porque una restricción lo está utilizando)
```sql
SELECT constraint_name
FROM user_constraints
WHERE table_name = 'ALUMNOS' AND constraint_type = 'U' AND index_name = 'I_ALUMNOS_DOCUMENTO';

```
10. Elimine la restricción única establecida sobre "documento".
```sql
ALTER TABLE alumnos
DROP CONSTRAINT UQ_alumnos_documento;

```
11. Verifique que el índice "I_alumnos_documento" aún existe.
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'IDX_documento';

```
12. Elimine el índice "I_alumnos_documento", ahora puede hacerlo porque no hay restricción que lo utilice.
```sql
DROP INDEX IDX_documento;

```
13. Elimine el índice "I_alumnos_nombre".
```sql
DROP INDEX IDX_nombre;

```
14. Elimine la restricción "primary key"/
```sql
ALTER TABLE alumnos
DROP CONSTRAINT PK_alumnos_legajo;

```
15. Verifique que el índice "PK_alumnos_legajo" fue eliminado (porque fue creado por Oracle al establecerse la restricción)
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS'

```
16. Cree un índice compuesto por los campos "curso" y "materia", no único.
```sql
CREATE INDEX IDX_curso_materia ON alumnos(curso, materia);

```
17. Verifique su existencia.
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'IDX_curso_materia';

```
18. Elimine la tabla "alumnos" y verifique que todos los índices han sido eliminados junto con ella.
```sql
DROP TABLE alumnos;

```