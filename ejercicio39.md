```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    preciomin number(5,2),
    preciomay number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22,20);
insert into libros values (2,'El quijote','Cervantes','Emece',15,13);
insert into libros values (3,'Aprenda PHP','Mario Molina','Siglo XXI',53,48);
insert into libros values (4,'Java en 10 minutos','Garcia','Siglo XXI',35,40);

alter table libros
add constraint PK_libros_codigo
primary key (codigo);

alter table libros
add constraint UQ_libros
unique (titulo,codigo,editorial);

alter table libros
add constraint CK_libros_precios_positivo
check (preciomin>=0 and preciomay>=0);

insert into libros values (5,'Matematica estas ahi','Paenza','Siglo XXI',-15,30);

update libros set preciomay=-20 where titulo='Uno';

alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);

update libros set preciomay=30
where titulo='Java en 10 minutos';

alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);

select *from user_constraints where table_name='LIBROS';

insert into libros values (6,'El gato con botas',null,'Planeta',25,30);

select *from user_cons_columns where table_name='LIBROS';

```