# 17. Valores por defecto (default)

### Ejercico 01

#### Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.

##### 1. Elimine la tabla "visitantes"
```sql
drop table visitantes;
```
##### 2. Cree la tabla con la siguiente estructura:

```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar(11),
    mail varchar(30) default 'no tiene',
    montocompra number (6,2)
);
```

##### 3. Analice la información que retorna la siguiente consulta:

```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'VISITANTES';
```

Todos los campos admiten valores nulos; hay 3 campos con valores predeterminados.

##### 4. Ingrese algunos registros sin especificar valores para algunos campos para ver cómo opera la cláusula "default":

```sql
insert into visitantes (domicilio,ciudad,telefono,mail,montocompra)
values ('Colon 123','Cordoba','4334455','<juanlopez@hotmail.com>',59.80);
insert into visitantes (nombre,edad,sexo,telefono,mail,montocompra)
values ('Marcos Torres',29,'m','4112233','<marcostorres@hotmail.com>',60);
insert into visitantes (nombre,edad,sexo,domicilio,ciudad)
values ('Susana Molina',43,'f','Bulnes 345','Carlos Paz');
```

##### 5. Recupere todos los registros.

```sql
SELECT * FROM visitantes;
```
Salida del Script

```sh
		f	Colon 123	Cordoba	4334455	<juanlopez@hotmail.com>	59,8
Marcos Torres	29	m		Cordoba	4112233	<marcostorres@hotmail.com>	60
Susana Molina	43	f	Bulnes 345	Carlos Paz		no tiene	
```
Los campos de aquellos registros para los cuales no se ingresó valor almacenaron el valor por defecto ("null" o el especificado con "default").

##### 6. Use la palabra "default" para ingresar valores en un "insert"
```sql
insert into visitantes (domicilio,ciudad,telefono,mail,montocompra)
values ('Chucuito 123',default,'4334455',default,70.80);
```
##### 7. Recupere el registro anteriormente ingresado.
```sql
select *from visitantes where domicilio='Chucuito 123';
```
Salida del Script

```sh
		f	Chucuito 123	Cordoba	4334455	no tiene	70,8
```
### Ejercico 02

#### Una pequeña biblioteca de barrio registra los préstamos de sus libros en una tabla llamada "prestamos". En ella almacena la siguiente información: título del libro, documento de identidad del socio a quien se le presta el libro, fecha de préstamo, fecha en que tiene que devolver el libro y si el libro ha sido o no devuelto.

##### 1. Elimine la tabla "prestamos"
```sql
drop table prestamos;
```
##### 2. Cree la tabla:

```sql
create table prestamos(
    titulo varchar2(40) not null,
    documento char(8) not null,
    fechaprestamo date not null,
    fechadevolucion date,
    devuelto char(1) default 'n'
);
```

##### 3. Consulte "user_tab_columns" y analice la información.

```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'PRESTAMOS';
```
Salida del Script

```sh
TITULO	N	
DOCUMENTO	N	
FECHAPRESTAMO	N	
FECHADEVOLUCION	Y	
DEVUELTO	Y	"'n'
"
```
Hay 3 campos que no admiten valores nulos y 1 solo campo con valor por defecto.

##### 4. Ingrese algunos registros omitiendo el valor para los campos que lo admiten.
```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto)
values ('ingeniero','12345678','12/05/2023','','s');

insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto)
values ('limpiador','12345679','12/12/2023','','');
```
##### 5. Seleccione todos los registros.
```sql
SELECT * FROM prestamos;
```
##### 6. Ingrese un registro colocando "default" en los campos que lo admiten y vea cómo se almacenó.
```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto)
values ('luchador','12345671','12/01/2023','',DEFAULT);

SELECT * FROM prestamos;
```
Salida del Script
```sh
ingeniero	12345678	12/05/23		s
limpiador	12345679	12/12/23		
luchador	12345671	12/01/23		n
```
##### 7. Intente ingresar un registro colocando "default" como valor para un campo que no admita valores nulos y no tenga definido un valor por defecto.
```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto)
values ('jugador','12345677',DEFAULT,DEFAULT,DEFAULT);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PRESTAMOS"."FECHAPRESTAMO").
```sh
Error que empieza en la línea: 94 del comando :
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto)
values ('jugador','12345677',DEFAULT,DEFAULT,DEFAULT)
Informe de error -
ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PRESTAMOS"."FECHAPRESTAMO")
```